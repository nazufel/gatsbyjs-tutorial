"use strict";

var path = require("path");

var _require = require("gatsby-source-filesystem"),
    createFilePath = _require.createFilePath;

exports.onCreateNode = function (_ref) {
  var node = _ref.node,
      getNode = _ref.getNode,
      actions = _ref.actions;
  var createNodeField = actions.createNodeField;

  if (node.internal.type === "MarkdownRemark") {
    var slug = createFilePath({
      node: node,
      getNode: getNode,
      basePath: "pages"
    });
    createNodeField({
      node: node,
      name: "slug",
      value: slug
    });
  }
};

exports.createPages = function _callee(_ref2) {
  var graphql, actions, createPage, result;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          graphql = _ref2.graphql, actions = _ref2.actions;
          createPage = actions.createPage;
          _context.next = 4;
          return regeneratorRuntime.awrap(graphql("\n    query {\n      allMarkdownRemark {\n        edges {\n          node {\n            fields {\n              slug\n            }\n          }\n        }\n      }\n    }\n  "));

        case 4:
          result = _context.sent;
          result.data.allMarkdownRemark.edges.forEach(function (_ref3) {
            var node = _ref3.node;
            createPage({
              path: node.fields.slug,
              component: path.resolve("./src/templates/blog-post.js"),
              context: {
                // Data passed to context is available
                // in page queries as GraphQL variables.
                slug: node.fields.slug
              }
            });
          });

        case 6:
        case "end":
          return _context.stop();
      }
    }
  });
};