"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = exports.options = exports.rhythm = exports.scale = void 0;

var _typography = _interopRequireDefault(require("typography"));

var _typographyThemeFairyGates = _interopRequireDefault(require("typography-theme-fairy-gates"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var typography = new _typography["default"](_typographyThemeFairyGates["default"]);
var scale = typography.scale,
    rhythm = typography.rhythm,
    options = typography.options;
exports.options = options;
exports.rhythm = rhythm;
exports.scale = scale;
var _default = typography;
exports["default"] = _default;