import React from 'react';
import { graphql, Link, useStaticQuery } from 'gatsby';

// define a function that takes routes and components
const ListLink = props => (
  <li style={{ display: `inline-block`, marginRight: `1rem` }}>
    <Link to={ props.to }>{ props.children }</Link>
  </li>
)

export default function Layout({ children }) {
  const siteTitle = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
      }
    `
  );

  return (
    <div style={{ margin: `3rem auto`, maxWidth: 1000, padding: `0 1rem` }}>
      <header style={{ marginBottom: `1.5rem`}}>
        <Link to="/" style={{ textShadow: `none`, backgroundImage: `none` }}>
        { siteTitle.site.siteMetadata.title }
        </Link>
        <ul style={{ listStyle: `none`, float: `right` }}>
          <ListLink to="/about/">About</ListLink>
          <ListLink to="/pandas/">Pandas</ListLink>
          <ListLink to="/panda-blog/">Panda Blog</ListLink>
          <ListLink to="/my-files/">My Files</ListLink>
          <ListLink to="/contact/">Contact</ListLink>
        </ul>
      </header>
      { children }
    </div>
  )
}